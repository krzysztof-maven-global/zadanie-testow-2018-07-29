<?php
namespace App\Service;


use Symfony\Component\DomCrawler\Crawler;

class SedocImageService
{
    const BASE_URL = "https://www.sedoc.pl";
    const UPLOAD_PATH =  __DIR__.'/../../public/';

    public function chooseRandomImage(): string
    {
        $crawler = new Crawler($this->fetchIndexHtml());

        $result = $crawler
            ->filterXpath('//img')
            ->extract(['src']);

        if (empty($result)) {
            throw new \LogicException('No image found');
        }

        return $result[array_rand($result)];
    }

    public function download(string $imageSrc): string
    {
        $fileName = uniqid();
        $content = file_get_contents(self::BASE_URL.$imageSrc);
        file_put_contents(self::UPLOAD_PATH.$fileName, $content);

        return $fileName;
    }

    public function compareFiles(string $localFileName, string $imageSrc): bool
    {
        return hash_file('sha1', self::UPLOAD_PATH.$localFileName) === hash_file(
            'sha1', self::BASE_URL.$imageSrc
        );
    }

    public function getAbsoluteUrl(string $path): string
    {
        return self::BASE_URL.$path;
    }

    protected function fetchIndexHtml(): string
    {
        return file_get_contents(self::BASE_URL);
    }
}