<?php
namespace App\Controller;

use App\Service\SedocImageService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function index(SedocImageService $sedocImageService)
    {
        $sedocImageSrc      = $sedocImageService->chooseRandomImage();
        $downloadedFileName = $sedocImageService->download($sedocImageSrc);
        $isFilesEquals      = $sedocImageService->compareFiles($downloadedFileName, $sedocImageSrc);
        $sedocImageUrl      = $sedocImageService->getAbsoluteUrl($sedocImageSrc);

        return $this->render('index.html.twig', compact(
            'sedocImageUrl',
            'downloadedFileName',
            'isFilesEquals'
        ));
    }
}